import 'dart:io';

List<String> listInfix = <String>[];
List<String> listPostfix = <String>[];
List<String> listOperators = <String>[];
List<int> listValues = <int>[];

//Infix
List<String> Infix(var a) {
  listInfix = a.split(' ');
  return listInfix;
}

//infix to Postfix
List<String> InfixToPostfix(List listToken) {
  for (int i = 0; i < listToken.length; i++) {
    //add integer
    if (int.tryParse(listToken[i]) != null) {
      listPostfix.add((listToken[i]));
    }
    //check operator and add to listOperators and listPostfix
    if (listToken[i] == "+" ||
        listToken[i] == "-" ||
        listToken[i] == "*" ||
        listToken[i] == "/" ||
        listToken[i] == "^") {
      // check + and -
      while (listOperators.isNotEmpty &&
          listOperators.last != "(" &&
          (listToken[i] == "+" ||
              listToken[i] == "-" && listOperators.last == "*" ||
              listOperators.last == "/" ||
              listOperators.last == "^")) {
        listPostfix.add(listOperators.removeLast());
      }
      //check * , / and ^
      while (listOperators.isNotEmpty &&
          listOperators.last != "(" &&
          (listToken[i] == "*" ||
              listToken[i] == "/" && listOperators.last == "*" ||
              listOperators.last == "/" ||
              listOperators.last == "^")) {
        listPostfix.add(listOperators.removeLast());
      }
      listOperators.add(listToken[i]);
    }
    //check open ( and add to listOperators
    if (listToken[i] == "(") {
      listOperators.add(listToken[i]);
    }
    //check close ) and add to listPostfix and remove open (
    if (listToken[i] == ")") {
      while (!(listOperators.last == "(")) {
        listPostfix.add(listOperators.removeLast());
      }
      listOperators.removeLast();
    }
  }
  //add operators to Postfix
  while (listOperators.isNotEmpty) {
    listPostfix.add(listOperators.removeLast());
  }
  return listPostfix;
}

//Postfix
List<int> Postfix(List listEvaluate) {
  int sum = 0;
  for (int i = 0; i < listEvaluate.length; i++) {
    if (int.tryParse(listEvaluate[i]) != null) {
      listValues.add(int.parse(listEvaluate[i]));
    } else {
      int right = listValues.removeLast();
      int left = listValues.removeLast();
      if (listEvaluate[i] == "+") {
        sum = left + right;
        listValues.add((sum));
      } else if (listEvaluate[i] == "-") {
        sum = left - right;
        listValues.add((sum));
      } else if (listEvaluate[i] == "*") {
        sum = left * right;
        listValues.add((sum));
      } else if (listEvaluate[i] == "/") {
        sum = left ~/ right;
        listValues.add((sum));
      } else if (listEvaluate[i] == "^") {
        sum = left;
        for (int i = 1; i < right; i++) {
          sum *= left;
        }
        listValues.add((sum));
      }
    }
  }
  return listValues;
}

void main() {
  //input
  print("Input Expression : ");
  String input = stdin.readLineSync()!;

  //infix
  print("Infix : ");
  Infix(input);
  print(listInfix);

  //infix to postfix
  print("Infix to Postfix : ");
  InfixToPostfix(listInfix);
  print(listPostfix);

  //postfix & result
  print("Evaluate Postfix : ");
  Postfix(listPostfix);
  print(listValues);
}
